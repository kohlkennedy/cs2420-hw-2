// BinarySearchTree class
//
// CONSTRUCTION: with no initializer
// ******************ERRORS********************************
// Throws UnderflowException as appropriate
/**
*  Implements an unbalanced binary search tree.
*
*  @author Me
*/

import java.util.Random;

import java.util.ArrayList;

class UnderflowException extends RuntimeException {
	/**
	 * Construct this exception object.
	 *
	 * @param message the error message.
	 */
	public UnderflowException(String message) {
		super(message);
	}
}

public class Tree<E extends Comparable<? super E>> {
	final String ENDLINE = "\n";

	/**
	 * Public method to create an empty tree
	 */
	public Tree(String label) {
		treeName = label;
		root = null;
	}

	/**
	 * Public method to create a tree from ArrayList of elements
	 * 
	 * @param arr list of items to add to the tree
	 */
	public Tree(ArrayList<E> arr, String label) {
		root = null;
		treeName = label;
		for (int i = 0; i < arr.size(); i++) {
			insert(arr.get(i));
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * Public method to create a tree from array of elements
	 * 
	 * @param arr list of items to add to the tree
	 */
	public Tree(E[] arr, String label) {
		root = null;
		treeName = label;
		for (int i = 0; i < arr.length; i++) {
			insert(arr[i]);
		}
	}

	/**
	 * Public method to count leaf nodes
	 * 
	 * @return number of leaf nodes
	 */
	public int countFringe() {
		return countFringe(root);
	}

	/**
	 * Public method to find predecessor of the curr node. Uses curr, a local
	 * variable set by contains.
	 * 
	 * @return String representation of predecessor
	 */
	public String predecessor() {
		if (curr == null)
			curr = root;
		curr = predecessor(curr);
		if (curr == null)
			return "null";
		else
			return curr.toString();
	}

	/**
	 * Insert into the tree; duplicates are allowed
	 *
	 * @param x the item to insert.
	 */
	public void insert(E x) {
		root = insert(x, root, null);
	}

	/**
	 * Find an item in the tree.
	 *
	 * @param v the item to search for.
	 * @return true if found.
	 */
	public boolean contains(E v) {
		return contains(v, root);
	}

	/**
	 * Find an item in the tree.
	 *
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param v the item to search for.
	 * @return the closest node if not found.
	 */
	public BinaryNode<E> findClosest(E v) {
		return findClosest(root, v);
	}

	/**
	 * Make the tree logically empty.
	 */
	public void makeEmpty() {
		root = null;
	}

	/**
	 * Make a deep copy of the tree.
	 */
	public void clone(Tree<E> t) {
		this.root = cloneIt(t.root, this.root);
	}

	/**
	 * Return a string displaying the tree contents as a tree.
	 */
	public String prettyTree() {
		if (root == null)
			return (treeName + " Empty tree\n");
		else
			return treeName + ENDLINE + prettyTree(root, "  ");
	}

	/**
	 * Return a string displaying the tree contents as a single line
	 */
	public String traverse() {
		if (root == null)
			return treeName + " Empty tree";
		else
			return treeName + " " + traverse(root);
	}

	/**
	 * Returns number of nodes in the tree
	 *
	 */
	public int countNodes() {
		return countNodes(root);
	}

	//Returns number of node in a level
	public int nodesInLevel(int level) {
		return nodesInLevel(root, level);

	}

	//Returns the node in the Kth place
	public String findKthInOrder(int k) {
		BinaryNode<E> kth = findKthInOrder(root, k);
		if (kth == null)
			return "NONE";
		else
			return kth.toString();
	}

	//Returns if a tree is isomorphic or not
	public boolean isIsomorphic(Tree<E> t2) {
		return isIsomorphic(this.root, t2.root);
	}

	/**
	 * Internal method to determine if two trees are isomorphic This routine runs in
	 * O(??)
	 * 
	 * @param t1 one tree
	 * @param t2 second tree
	 * @return true if t2 and t2 are isomorphic
	 */
	private boolean isIsomorphic(BinaryNode<E> t1, BinaryNode<E> t2) {
		//If the branches being looked at are both null, they are the same
		if (t1 == null && t2 == null)
			return true;
		//If one node is null, while the other isn't, then they are not equal
		if (t1 == null || t2 == null)
			return false;
		//Sets both branches of the tree to variables
		boolean leftBranch = isIsomorphic(t1.left, t2.left);
		boolean rightBranch = isIsomorphic(t1.right, t2.right);
		//If both branches are true, then true will be returned, otherwise it'll be false
		return leftBranch && rightBranch;
	}

	/** 
	 * Public method for the least common ancestor
	 *
	 * @param t the first element being searched
	 * @param t2 the second element
	 * @return the ancestor, or if none, then null
	 */
	public String lca(E t, E t2)
	{
		BinaryNode<E> tBinaryNode = findClosest(root, t);
		BinaryNode<E> t2BinaryNode = findClosest(root, t2);
		BinaryNode<E> lcaBinaryNode = leastCommonAncenstor(tBinaryNode,t2BinaryNode);
		if (lcaBinaryNode == null)
			return "NONE";
		else
			return lcaBinaryNode.toString();
	}

	/** 
	 * Public method for the least common ancestor
	 *
	 * @param t1 the first node
	 * @param t2 the second node
	 * @return ancestor node
	 */
	private BinaryNode<E> leastCommonAncenstor(BinaryNode<E> t1, BinaryNode<E> t2)
	{	
		//If one of them is null, return null
		if(t1 == null || t2 == null)
			return null;
		//If one of the elements is the root, then return the root
		else if (t1 == root || t2 == root)
			return root;
		//If they are equal, return one of the values
		else if (t1 == t2)
			return t1;
		//t1 is equal to the parent of t2, return t1
		else if(t1 == t2.parent)
			return t1;
		//t2 is equal to the parent of t1, return t2
		else if (t2 == t1.parent)
			return t2;
		else
		{
			//If the two nodes are on the same level, but aren't equal or have the same parent, then go up a level
			if(t1.level == t2.level)
				return leastCommonAncenstor(t1.parent, t2.parent);
			//If t1 is a lower level than t2, then raise t1 a level and keep t2 at the same level
			if(t1.level < t2.level)
				return leastCommonAncenstor(t1.parent, t2);
			//If t2 is a lower level than t1, then raise t2 a level and keep t1 at the same level
			else
				return leastCommonAncenstor(t1, t2.parent);
		}
	}
	
	/**
	 * Internal method to determine if two trees are quasi- isomorphic This routine
	 * runs in O(??)
	 * 
	 * @param t1 one tree
	 * @param t2 second tree
	 * @return true if t2 and t2 are quasi isomorphic
	 */
	private boolean isQuasiIsomorphic(BinaryNode<E> t1, BinaryNode<E> t2) {
		if (t1 == null && t2 == null)
			return true;
		if (t1 == null || t2 == null)
			return false;
		//Divides the branch in half and sets each one to a varible
		boolean leftBranch = isQuasiIsomorphic(t1.left, t2.right);
		boolean rightBranch = isQuasiIsomorphic(t1.right, t2.left);
		//If both branches returns true, then they are quasi-isomorphic
		return leftBranch == true && rightBranch == true;
	}

	//The public method for the isQuasiIsomorphic method
	public boolean isQuasiIsomorphic(Tree<E> t2) {
		return isQuasiIsomorphic(this.root, t2.root);
	}

	/* PRIVATE */
	/**
	 * This routine runs in O(??)
	 * 
	 * @param t the root of the tree
	 */
	private int countFringe(BinaryNode<E> t) {
		if (t == null)
			return 0;
		if (t.left == null && t.right == null)
			return 1;
		else
			return (countFringe(t.left) + countFringe(t.right));
	}

	/**
	 * This routine runs in O(??)
	 * 
	 * @param t the root of the tree
	 */

	private int countNodes(BinaryNode<E> t) {
		if (t == null)
			return 0;
		return (countNodes(t.left) + countNodes(t.right) + 1);
	}

	/**
	 * This routine runs in O(??)
	 * 
	 * @param t the root of the tree
	 */
	private BinaryNode<E> predecessor(BinaryNode<E> t) {
		//If t is null, return null
		if (t == null)
			return null;
		
		BinaryNode<E> t2 = t.parent;
		if(t2.left == null || t2.left == t)
		{
			return t2;
		}
		BinaryNode<E> curr = t2.left;
		while(curr.right != null)
		{
			curr = curr.right;
		}
		return curr;
		
	}

	/**
	 *
	 * Internal method to insert into a subtree. This routine runs in O(??)
	 * 
	 * @param x the item to insert.
	 * @param t the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private BinaryNode<E> insert(E x, BinaryNode<E> t, BinaryNode<E> parent) {
		if (t == null){
			return new BinaryNode<>(x, null, null, parent, 0, 0);}
		int compareResult = x.compareTo(t.element);
		if (compareResult < 0) {
			t.leftSize++;
		
			t.left = insert(x, t.left, t);
			if(t.parent != null)
				t.level = t.parent.level + 1;
			
		} else {
			
			t.rightSize++;
			t.right = insert(x, t.right, t);
			if(t.parent != null)
				t.level = t.parent.level + 1;
			
		}
		
		
		return t;
	}

	/**
	 * Internal method to find an item in a subtree.
	 *
	 * This routine runs in O(log n) as there is only one recursive call that is
	 * executed and the work associated with a single call is independent of the
	 * size of the tree: a=1, b=2, k=0
	 * 
	 * @param x is item to search for.
	 * @param t the node that roots the subtree. SIDE EFFECT: Sets local variable
	 *          curr to be the node that is found
	 * @return node containing the matched item.
	 */
	private boolean contains(E x, BinaryNode<E> t) {
		curr = null;
		if (t == null)
			return false;
		int compareResult = x.compareTo(t.element);
		if (compareResult < 0)
			return contains(x, t.left);
		else if (compareResult > 0)
			return contains(x, t.right);
		else {
			curr = t;
			return true; // Match
		}
	}

	/**
	 * Internal method to find an item or the closest node in a subtree. This
	 * routine runs in O(??)
	 * 
	 * @param v is item to search for.
	 * @param t the node that roots the subtree.
	 * @return node containing the matched item.
	 */
	private BinaryNode<E> findClosest(BinaryNode<E> t, E v) {
		//If t is null, return null
		if (t == null)
			return t;
		//If the nodes element is lesser than searched element, go left
		if (t.element.compareTo(v) > 0)
			//If the left child is null, then this is the smallest element and return it
			if(t.left == null)
				return t;
			//If not, keep searching
			else
				return findClosest(t.left, v);
		//If the element equals v, then return the node
		else if (t.element.equals(v))
			return t;
		else
			if(t.right == null)
				return t;
			else
				return findClosest(t.right, v);

	}

	/**
	 * Internal method to print a subtree in sorted order. This routine runs in
	 * O(n)
	 * 
	 * @param t      the node that roots the subtree.
	 * @param indent a string of blanks associated with the level of the node
	 */
	private String prettyTree(BinaryNode<E> t, String indent) {
		StringBuilder sb = new StringBuilder();
		sb.append(indent);
		if(t.right != null)	
		{	
			//for(int i = 0; i < t.level; i++)
				//sb.append(indent);
			sb.append(prettyTree(t.right,indent));
			for(int i = 0; i < t.level; i++)
				sb.append(indent);
		}
		sb.append(t.element);
		if (t.parent == null) {
			sb.append(" < No parent ");
		} else {
			sb.append("<");
			sb.append(t.parent.element);
		}
		sb.append("> [");
		
		sb.append(t.leftSize);
		sb.append(",");
		sb.append(t.rightSize);
		sb.append("]\n");
		sb.append(indent);
		if(t.left != null)	
		{
			for(int i = 0; i < t.level; i++)
				sb.append(indent);
			
			sb.append(prettyTree(t.left,indent));
			//for(int i = 0; i < t.level; i++)
				//sb.append(indent);	
		}
		return sb.toString();
	}

	/**
	 * Internal method to return a string of items in the tree in order This routine
	 * runs in O(n)
	 * 
	 * @param t the node that roots the subtree.
	 */
	private String traverse(BinaryNode<E> t) {
		if (t == null)
			return "";
		StringBuilder sb = new StringBuilder();
		sb.append(traverse(t.left));
		sb.append(t.element.toString() + " ");
		sb.append(traverse(t.right));
		return sb.toString();
	}

	/**
	 * Internal method to count number of nodes at level This routine runs in O(??)
	 * 
	 * @param t the node that roots the subtree.
	 * @param   level, root is level 0
	 * @return number of nodes in subtree t at level
	 */

	private int nodesInLevel(BinaryNode<E> t, int level) {
		if (t == null)
		  return 0;
	  
		if(level == 0)
		  return 1;
	  
		return (nodesInLevel(t.left, level - 1) + nodesInLevel(t.right, level - 1));
	  }

	/**
	 * Internal method to find the kth value in the tree (by order This routine runs
	 * in O(log n)
	 * 
	 * @param t the node that roots the subtree.
	 * @param   k, which item is wanted, by order
	 * @return kth successor node
	 */

	private BinaryNode<E> findKthInOrder(BinaryNode<E> t, int k) {
		if (t == null) {
			return null;
		}
		if(t.leftSize == k -1)
			return t;
		if(t.leftSize < k - 1)
		{
			k = k - t.leftSize - 1;
			return findKthInOrder(t.right, k) ;
		}
		else
			return findKthInOrder(t.left, k);

	}

	/**
	 * Internal method to determine clone the tree This routine runs in O(n)
	 * 
	 * @param t      tree
	 * @param parent parent node of tree to be created
	 * @return cloned tree
	 */
	private BinaryNode<E> cloneIt(BinaryNode<E> t, BinaryNode<E> parent) {
		if (t == null) {
			//parent = null;
			return null;
		} else {
			BinaryNode<E> new_tree = t;
			parent = new_tree;
			parent.left = cloneIt(new_tree.left, parent.left);
			parent.right = cloneIt(new_tree.right, parent.right);
			return parent;
		}
	}

	//Public getter for the width function
	public int width()
	{
		return getWidth(this.root);
	}
	//Recursive call for the getWidth function
	private int getWidth(BinaryNode<E> curr)
	{
		if(curr == null)
			return 0;
		

		return leftBranch(curr.left) + rightBranch(curr.right) + 1;
	}

	private int leftBranch(BinaryNode<E> t)
	{
		if(t == null)
			return 0;
		else	
			if(t.leftSize > t.rightSize)
				return leftBranch(t.left) + 1;
			else
				return leftBranch(t.right) + 1;
	}

	private int rightBranch(BinaryNode<E> t)
	{
		if(t == null)
			return 0;
		else	
			if(t.leftSize > t.rightSize)
				return rightBranch(t.left) + 1;
			else
				return rightBranch(t.right) + 1;
	}

	/*private int max(int a, int b)
	{
		if (a < b)
			return a;
		else	
			return b;
	}*/

	// Basic node stored in unbalanced binary search trees
	private static class BinaryNode<AnyType> {
		// Constructors
		BinaryNode(AnyType theElement) {
			this(theElement, null, null, null, 0, 0);
		}

		BinaryNode(AnyType theElement, BinaryNode<AnyType> lt, BinaryNode<AnyType> rt, BinaryNode<AnyType> pt,
				int leftSize, int rightSize) {
			element = theElement;
			left = lt;
			right = rt;
			this.leftSize = leftSize;
			this.rightSize = rightSize;
			parent = pt;
		}

		AnyType element; // The data in the node
		BinaryNode<AnyType> left; // Left child

		BinaryNode<AnyType> right; // Right child
		BinaryNode<AnyType> parent; // Parent node
		int leftSize;
		int rightSize;
		int level;

		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("Node:");
			sb.append(element);
			if (parent == null) {
				sb.append("< No Parent");
			} else {
				sb.append("<");
				sb.append(parent.element);
			}
			sb.append("> [");
			sb.append(leftSize);
			sb.append(",");
			sb.append(rightSize);
			sb.append("]");
			return sb.toString();
		}
	}

	/**
	 * The tree root.
	 */
	private BinaryNode<E> root;
	private BinaryNode<E> curr;
	private String treeName;

	// Test program
	public static void main(String[] args) {
		long seed = 436543;
		Random generator = new Random(seed); // Don't use a seed if you want the numbers to be different each time
		final String ENDLINE = "\n";
		int val = 460;
		final int SIZE = 8;
		Integer[] v0 = { 25, 10, 60, 55, 45 };
		Integer[] v7 = { 30, 15, 65, 75, 83 };
		Integer[] v1 = { 25, 10, 60, 55, 58, 56, 14, 10, 75, 80, 20, 10, 5, 7, 61, 62, 63 };
		ArrayList v2 = new ArrayList();
		ArrayList v3 = new ArrayList();
		ArrayList v4 = new ArrayList();
		ArrayList v5 = new ArrayList();
		ArrayList v6 = new ArrayList();
		for (int i = 0; i < SIZE; i++) {
			int t = generator.nextInt(100);
			// System.out.println( " t is " + t );
			v2.add(t);
			v3.add(t + generator.nextInt(5));
			v4.add(t + 18);
			v5.add(100 - t);
		}
		for (int i = 0; i < SIZE * SIZE; i++) {
			int t = generator.nextInt(2000);
			v6.add(t);
		}
		v6.add(val);
		Tree<Integer> tree0 = new Tree<Integer>(v0, "Tree0:");
		Tree<Integer> tree1 = new Tree<Integer>(v1, "Tree1:");
		Tree<Integer> tree2 = new Tree<Integer>(v2, "Tree2:");
		Tree<Integer> tree3 = new Tree<Integer>(v3, "Tree3:");
		Tree<Integer> tree4 = new Tree<Integer>(v4, "Tree4:");
		Tree<Integer> tree5 = new Tree<Integer>(v5, "Tree5:");
		Tree<Integer> tree6 = new Tree<Integer>(v6, "Tree6:");
		Tree<Integer> tree7 = new Tree<Integer>(v7, "Tree7:");
		Tree<Integer> tree8 = new Tree<Integer>("Tree8:");
		System.out.print(tree0.prettyTree());
		tree8.clone(tree0);
		System.out.println("Not destroyed after the clone" + tree0.traverse());
		System.out.println("Traverse: " + tree8.traverse());
		System.out.println(tree8.prettyTree());
		tree8.makeEmpty();
		System.out.println("Now Empty" + tree8.prettyTree());
		System.out.println("Not destroyed" + tree0.traverse());
		System.out.println(tree1.prettyTree());
		System.out.println("Fringe count = " + tree1.countFringe());
		System.out.println(tree6.prettyTree());
		System.out.println("Size of Tree 6 " + tree6.countNodes() + ENDLINE);
		tree6.contains(val); // Sets the current node inside the tree6 class.
		System.out.println("In Tree6, starting at " + val + ENDLINE);
		System.out.println(tree6.prettyTree());
		int predCount = 5;
		// how many predecessors do you want to see?
		for (int i = 0; i < predCount; i++) {
			System.out.println("The next predecessor is " + tree6.predecessor());
		}
		System.out.println(tree4.prettyTree());
		System.out.print("The level count of tree4 at the root is: " + tree4.root.level + " element of said node: " + tree4.root.element);
		System.out.println("Number nodes at level " + 0 + " is " + tree4.nodesInLevel(0));
		int myLevel = 3;
		System.out.println("Number nodes at level " + myLevel + " is " + tree4.nodesInLevel(myLevel));
		myLevel = 4;
		System.out.println("Number nodes at level " + myLevel + " is " + tree4.nodesInLevel(myLevel));
		System.out.println(tree1.prettyTree());
		int k = 1;
		System.out.println("In tree1, the " + k + "th smallest value is " + tree1.findKthInOrder(k));
		k = 7;
		System.out.println("In tree1, the " + k + "th smallest value is " + tree1.findKthInOrder(k));
		k = 12;
		System.out.println("In tree1, the " + k + "th smallest value is " + tree1.findKthInOrder(k));
		System.out.println("The width of  tree1 is " + tree1.width() + ENDLINE);
		System.out.println(tree2.prettyTree());
		System.out.println("The width of  tree2 is " + tree2.width() + ENDLINE);
		System.out.println(tree3.prettyTree());
		System.out.println(tree4.prettyTree());
		System.out.println(tree5.prettyTree());
		if (tree2.isIsomorphic(tree3))
			System.out.println("Trees 2 and 3 are Isomorphic");
		if (tree2.isIsomorphic(tree4))
			System.out.println("Trees 2 and 4 are Isomorphic");
		if (tree3.isIsomorphic(tree4))
			System.out.println("Trees 3 and 4 are Isomorphic");
		if (tree0.isIsomorphic(tree1))
			System.out.println("Trees 2 and 1 Are Isomorphic");
		if (tree2.isQuasiIsomorphic(tree3))
			System.out.println("Trees 2 and 3 Are Quasi-Isomorphic");
		if (tree2.isQuasiIsomorphic(tree5))
			System.out.println("Trees 2 and 5 Are Quasi-Isomorphic");
		if (tree2.isQuasiIsomorphic(tree5))
			System.out.println("Trees 2 and 4 Are Quasi-Isomorphic");
		if (tree0.isQuasiIsomorphic(tree7))
			System.out.println("Trees 0 and 7 Are Quasi-Isomorphic");
		System.out.println(tree1.prettyTree());
		System.out.println(tree1.root.left.level);
		System.out.println("Least Common Ancestor of (56,61) " + tree1.lca(56, 61) + ENDLINE);
		System.out.println("Least Common Ancestor (58,55) " + tree1.lca(58, 55) + ENDLINE);

	}
}